<?php

//extend php unit test frame work
class FailTest extends \PHPUnit_Framework_TestCase
{
  public function testEmailVariableContainCorrectValues()
  {
    $user= new \App\Models\User;
    $user->setFirstName('Mo');
    $user->setLastName('sa3d');
    $user->setEmail('sa3d@gmail.com');

    $emailVariables = $user->getEmailVariables();

    // han3ml test 3al key
    $this->assertArrayHasKey('full_name',$emailVariables);
    $this->assertArrayHasKey('email',$emailVariables);

    $this->assertEquals($emailVariables['full_name'],'Mosa3d');
    $this->assertEquals($emailVariables['email'],'sa3d@gmail.com');

  }

}
