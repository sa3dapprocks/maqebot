<?php

namespace App\Controller;

interface parserInterface{

public function parse(string $input) : array;

}
