<?php

namespace App\Models;

use \App\Controller;
use \App\Controller\WalkingController;
use \App\Controller\PostionController;
use \App\Controller\Parser;

class Robot implements robotInterface{
private $x_axis;
private $y_axis;
private $myFrontIs;
private $tokens=[];
public function __construct(){
  $this->x_axis = 0;
  $this->y_axis = 0;
  $this->myFrontIs = 0;
}


public function iamLookingToward()
{
  return $this->myFrontIs;
}

public function moveMeRight()
{
  $postioner = new PostionController;
  $this->myFrontIs = $postioner->MoveRight($this->myFrontIs);
  return $this->myFrontIs ;
}

public function moveMeLeft()
{

  $postioner = new PostionController;
  $this->myFrontIs = $postioner->MoveLeft($this->myFrontIs);
  return $this->myFrontIs ;
}

public function walk(int $steps)
{
  if($this->myFrontIs==0 || $this->myFrontIs==2)
  {
  $this->y_axis = $this->y_axis+1;
  }

  else {
    $this->x_axis = $this->x_axis+1;

  }

}

public function finalLocation(){
  return " X-axis : ".$this->x_axis." y-axis : ".$this->y_axis." Direction : ".$this->iamLookingToward();;
}

public function parse (string $input)
{
  $parser = new Parser;
  $this->tokens = $parser->parse($input);
}

public function exeCommand(){
  foreach($this->tokens as $token){

      switch ($token[0]) {
        case 'R':
          $this->moveMeRight();
          break;

        case 'L':
          $this->moveMeLeft();
          break;

        case 'W':
          $this->walk(1);
          break;

        default:
          // code...
          //$this->myFrontIs=200;
          break;
      }
    }
}

public function getTokens()
{
  return $this->tokens;
}

}
