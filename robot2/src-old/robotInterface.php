<?php

namespace App\Models;

interface robotInterface{


  public function iamLookingToward();
  public function moveMeRight();
  public function moveMeLeft();
  public function walk(int $steps);
  public function finalLocation();

}
