<?php

namespace App\Models;

class User{
public $first_name;
public $last_name;
public $email;

public function setFirstName($first_name)
{
  $this->first_name=$first_name;
}

public function getFirstName(){
  return $this->first_name;
}

// setup last name

public function setLastName($last_name)
{
  $this->last_name=$last_name;
}

public function getLastName()
{
  return $this->last_name;
}

public function getFullName(){
   $fullname=$this->first_name.$this->last_name;

  return $fullname;
}

public function setEmail($email){
  $this->email=$email;
}

public function getEmail(){
  return $this->email;
}

public function getEmailVariables()
{
  return [
    'full_name'=>'Mosa3d',
    'email'=>$this->email
  ];
}

}
