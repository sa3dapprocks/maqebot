<?php

namespace App\Support;

class Collection{

  protected $items = [];

  public function __construct(array $items=[])
{
    $this->items=$items;
}

  public function count(){
    return count($this->items);
  }

  public function geT(){
    return $this->items;
  }
}
