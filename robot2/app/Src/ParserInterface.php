<?php

namespace App\Src;

interface parserInterface{

private $token = array();
public function parse(string $input) : array;
public function getTokens();

}
