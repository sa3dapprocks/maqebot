<?php

//extend php unit test frame work
class CollectionTest extends \PHPUnit_Framework_TestCase
{

  /** @test */
  public function count_is_correct_for_items_passed_it()
  {
    $collection = new \App\Support\Collection([
      'one','two','three'
    ]);

    $this->assertEquals(3,$collection->count());
  }

  /** @test */
  public function items_returned_match_items_passed()
  {
    $collection= new \App\Support\Collection([
      'one','two','three'
    ]);

    $this->assertCount(3,$collection->get());
    $this->assertEquals($collection->get()[0],'one');
    $this->assertEquals($collection->get()[1],'two');
  }

}
