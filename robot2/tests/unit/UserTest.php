<?php

//extend php unit test frame work
class UserTest extends \PHPUnit_Framework_TestCase
{
  public function testThatWeCanGetTheFirstName()
  {
    $user = new \App\Models\User;
    $user->setFirstName('Mo');
    $this->assertEquals($user->getFirstName(),'Mo');
  }

  public function testThatWeCanGetTheLastName()
  {
    $user = new \App\Models\User;
    $user->setLastName('sa3d');
    $this->assertEquals($user->getLastName(),'sa3d');
  }

  public function testFullName()
  {
    $user = new \App\Models\User;
    $user->setFirstName('Mo');
    $user->setLastName('sa3d');

    $this->assertEquals($user->getFullName(),'Mosa3d');
  }

  public function testEmailAdess()
  {
    $user = new \App\Models\User;
    $email='sa3d@gmail.com';
    $user->setEmail($email);
    $this->assertEquals($user->getEmail(),$email);
  }


}
